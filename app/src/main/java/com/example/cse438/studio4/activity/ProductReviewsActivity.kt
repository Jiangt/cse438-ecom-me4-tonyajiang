package com.example.cse438.studio4.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.cse438.studio4.R
import com.example.cse438.studio4.adapter.ReviewAdapter
import com.example.cse438.studio4.model.Review
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_product_reviews.*

class ProductReviewsActivity: AppCompatActivity() {
    private var reviewList = ArrayList<Review>()
    private lateinit var adapter: ReviewAdapter

    private lateinit var itemId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_reviews)

        this.itemId = intent.getStringExtra("ItemId")


        adapter = ReviewAdapter(this, reviewList)
        review_items_list.adapter = adapter
    }

    override fun onStart() {
        super.onStart()

        val db = FirebaseFirestore.getInstance()

        // TODO: Finish implementing with given pseudocode
    }

    override fun onBackPressed() {
        this.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}